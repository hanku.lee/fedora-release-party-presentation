# Fedora Event Presentation

## Description
This project keeps presentation deck prepared by Docs team for Fedora Event.

## Roadmap
Presentation deck will be uploaded every 6 months according to the release schedule, and when required.

## Contributing
Refer to the contributor guide below if you need help with authoring process.

https://docs.fedoraproject.org/en-US/fedora-docs/contributing-docs/tools-gitlab-howto/

## Authors and acknowledgment
Credit to Peter Boy and Justin Flory

## License
All Fedora Documentation content available under CC BY-SA 4.0.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/hanku.lee/fedora-release-party-presentation.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hanku.lee/fedora-release-party-presentation/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

***


